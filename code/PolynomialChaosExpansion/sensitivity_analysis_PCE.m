%% Sensitivity bar plot of KM-SUB
% Plot sensitivities for 50% lifetime 
%%
clearvars;
%addpath(genpath('/Users/arifeinberg/Documents/MATLAB/my_UQLab_folder'))
%uqlab;
rng 'default' ;  % Fix random seed for repeatable results

%% Input data
% Because PCE requires a choice of polynomial basis, a probabilistic input
% model needs to be defined. Specify the marginals of the probabilistic
% input model:

%  Create the 10-dimensional stochastic input model using uniform
%  distributions

IOpts.Marginals(1).Name = 'RS1'; % surface reaction coefficient O3 + OL --> I
IOpts.Marginals(1).Type = 'Uniform';
IOpts.Marginals(1).Parameters = log10([1e-15 1e-10]); %for loguniform distribution

IOpts.Marginals(2).Name = 'RB1'; % bulk reaction rate coefficient  O3 + OL --> product + CI (RB1)
IOpts.Marginals(2).Type = 'Uniform';
IOpts.Marginals(2).Parameters = log10([1e-18 1e-13]); %for loguniform distribution

IOpts.Marginals(3).Name = 'dXY'; %diffusion coefficient of X in Y [cm2 s-1]
IOpts.Marginals(3).Type = 'Uniform';
IOpts.Marginals(3).Parameters = log10([1e-11 1e-5]); %for loguniform distribution

IOpts.Marginals(4).Name = 'dYY'; %self-diffusion coefficient of Y [cm2 s-1]
IOpts.Marginals(4).Type = 'Uniform';
IOpts.Marginals(4).Parameters = log10([1e-12 1e-6]); %for loguniform distribution

IOpts.Marginals(5).Name = 'HXY'; %Henry's law coefficient of X in Y []
IOpts.Marginals(5).Type = 'Uniform';
IOpts.Marginals(5).Parameters = log10([5e-6 5e-3]); %for loguniform distribution

IOpts.Marginals(6).Name = 'deX'; %desorption lifetime of X [s]
IOpts.Marginals(6).Type = 'Uniform';
IOpts.Marginals(6).Parameters = log10([1e-9 1e-2]); %for loguniform distribution

IOpts.Marginals(7).Name = 'acX'; %surface accommodation coefficient of X []
IOpts.Marginals(7).Type = 'Uniform';
IOpts.Marginals(7).Parameters = log10([1e-4 1]); %for loguniform distribution

IOpts.Marginals(8).Name = 'rad'; %outer particle radius [cm]
IOpts.Marginals(8).Type = 'Uniform';
IOpts.Marginals(8).Parameters = log10([2.5e-6 5e-3]); %for loguniform distribution

IOpts.Marginals(9).Name = 'gsX';%initial gas phase concentration of X [cm-3]
IOpts.Marginals(9).Type = 'Uniform';
IOpts.Marginals(9).Parameters = log10([1e11 5e15]); %for loguniform distribution

IOpts.Marginals(10).Name = 'gsY';%initial concentration of Y [cm-3]
IOpts.Marginals(10).Type = 'Uniform';
IOpts.Marginals(10).Parameters = log10([1e19 2e21]); %for loguniform distribution

% Create an INPUT object based on the specified marginals:
myInput = uq_createInput(IOpts);
%%
%load PCEs
tic
myPCE_50 = load('PCE_results_20k_Bootstrap_50.mat','myPCE');
myPCE_50 = myPCE_50.myPCE;
toc
%%

%sensitivity analysis
YSobolOpts.Type = 'Sensitivity';
YSobolOpts.Method = 'Sobol';
YSobolOpts.Model = myPCE_50;

% Set the maximum Sobol' index order to 2
YSobolOpts.Sobol.Order = 2;

% Create and add the sensitivity analysis to UQLab
PCESobolAnalysis = uq_createAnalysis(YSobolOpts);

%save total, first, and second order indices
Total_Sobol = PCESobolAnalysis.Results.Total;
First_Sobol = PCESobolAnalysis.Results.FirstOrder;
Second_Sobol = PCESobolAnalysis.Results.AllOrders{2};

%save LOO error of PCE
LOO_error = myPCE_50.Error.ModifiedLOO;
%%
close all
VarNames = {'k_{SLR}','k_{BR}','D_{b,X}','D_{b,Y}','H_{cp,X}','\tau_{d,X}','\alpha_{s,0}','r_p','[X]_{g,0}','[Y]_{b,0}'};

[Total_indices_sorted, sort_total_I] = sort( Total_Sobol, 'descend');
First_indices_sort = First_Sobol(sort_total_I);
Varnames_sort = VarNames(sort_total_I);
x2=1:length(Total_indices_sorted);
figure('Position',[100 100 1261 536])
All_indices = [First_indices_sort Total_indices_sorted-First_indices_sort];
bar(x2, All_indices, 'stacked')
xticks(x2)
xticklabels(Varnames_sort)
legend('First order effect', 'Interaction effect')
set(gca, 'Fontsize', 20)
%title('Sensitivity indices for Se lifetime', 'Fontsize', 18)
ylabel('Sobol index')
