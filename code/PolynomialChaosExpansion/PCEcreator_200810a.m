%% Introduction
% This is a very simple script to import a training data set, use a
% specific sample from the training data and calculate a PCE metamodel.

%% Initiation
% Clear all variables from the workspace
clearvars

Time_Calc=zeros(1,4); % Time to start UQLab (1), load data (2), create the
% PCE metamodel (3) and to calculate the sobol indices (4).

tic
addpath(genpath('Path')) % Use this to add input data path
uqlab % Starts uqlab
Time_Calc(1)=toc;

%% Retrieve data sets
% Load the specified number of sample from a given file

Nexp=200; % Number of samples for the metamodel training

% Load training data and use Data_Training variable
tic
Input_Training = load(fullfile('InputTable_Training_90_200810.mat'));
Data_Training=Input_Training.Input;

% Store the content of the training data file in two matrices
X = log10(Data_Training(1:Nexp, 1:10));
Y = log10(Data_Training(1:Nexp, 11));
Time_Calc(2)=toc;

%% Input data
% Definition of probabilistic input model and marginals

%  Create the 10-dimensional stochastic input model using uniform
%  distributions
IOpts.Marginals(1).Name = 'RS1'; % surface reaction coefficient
IOpts.Marginals(1).Type = 'Uniform';
IOpts.Marginals(1).Parameters = log10([1e-15 1e-10]);

IOpts.Marginals(2).Name = 'RB1'; % bulk reaction rate coefficient
IOpts.Marginals(2).Type = 'Uniform';
IOpts.Marginals(2).Parameters = log10([1e-18 1e-13]);

IOpts.Marginals(3).Name = 'dXY'; %diffusion coefficient of X in Y [cm2 s-1]
IOpts.Marginals(3).Type = 'Uniform';
IOpts.Marginals(3).Parameters = log10([1e-11 1e-5]);

IOpts.Marginals(4).Name = 'dYY'; %self-diffusion coefficient of Y [cm2 s-1]
IOpts.Marginals(4).Type = 'Uniform';
IOpts.Marginals(4).Parameters = log10([1e-12 1e-6]);

IOpts.Marginals(5).Name = 'HXY'; %Henry's law coefficient of X in Y
IOpts.Marginals(5).Type = 'Uniform';
IOpts.Marginals(5).Parameters = log10([5e-6 5e-3]);

IOpts.Marginals(6).Name = 'deX'; %desorption lifetime of X [s]
IOpts.Marginals(6).Type = 'Uniform';
IOpts.Marginals(6).Parameters = log10([1e-9 1e-2]);

IOpts.Marginals(7).Name = 'acX'; %surface accommodation coefficient of X []
IOpts.Marginals(7).Type = 'Uniform';
IOpts.Marginals(7).Parameters = log10([1e-4 1]);

IOpts.Marginals(8).Name = 'rad'; %outer particle radius [cm]
IOpts.Marginals(8).Type = 'Uniform';
IOpts.Marginals(8).Parameters = log10([2.5e-6 5e-3]);

IOpts.Marginals(9).Name = 'gsX';%initial gas phase concentration of X [cm-3]
IOpts.Marginals(9).Type = 'Uniform';
IOpts.Marginals(9).Parameters = log10([1e11 5e15]);

IOpts.Marginals(10).Name = 'gsY';%initial concentration of Y [cm-3]
IOpts.Marginals(10).Type = 'Uniform';
IOpts.Marginals(10).Parameters = log10([1e19 2e21]);

% Create INPUT object
myInput = uq_createInput(IOpts);

%% PCE Metamodel
% Select PCE as metamodel type
MetaOpts.Type = 'Metamodel';
MetaOpts.MetaType = 'PCE';

% Set the maximum polynomial degree to 14 and exclude truncation
MetaOpts.Degree = 2:14;
MetaOpts.TruncOptions.qNorm = 1;

% Use experimental design loaded from the data files:
MetaOpts.ExpDesign.X = X;
MetaOpts.ExpDesign.Y = Y;

% Create the metamodel object and add it to UQLab:
tic
myPCE = uq_createModel(MetaOpts);

% Print a summary of the resulting PCE metamodel:
uq_print(myPCE)
Time_Calc(3)=toc;

%% Sensitivity analysis
% Specify the sensitivity module and Sobol' indices
YSobolOpts.Type = 'Sensitivity';
YSobolOpts.Method = 'Sobol';

% Set the maximum Sobol' index order to 2
YSobolOpts.Sobol.Order = 2;

% Create and add the sensitivity analysis to UQLab
tic
PCESobolAnalysis = uq_createAnalysis(YSobolOpts);
Time_Calc(4)=toc;

%% Store Metamodel for further Sensitivity Analysis
save(sprintf("OLPCEeuler_90_Nexp%i", Nexp), "myPCE", "PCESobolAnalysis", "Time_Calc")