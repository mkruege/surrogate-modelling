%% Sensitivity map of KM-SUB
% Try different values of radius & O3, recalculate PCE and use this to
% calculate sensitivity analysis, then map what are the most important
% variables and where
%% Input data
% Because PCE requires a choice of polynomial basis, a probabilistic input
% model needs to be defined. Specify the marginals of the probabilistic
% input model:

%  Create the 10-dimensional stochastic input model using uniform
%  distributions

IOpts.Marginals(1).Name = 'RS1'; % surface reaction coefficient O3 + OL --> I
IOpts.Marginals(1).Type = 'Uniform';
IOpts.Marginals(1).Parameters = log10([1e-15 1e-10]); %for loguniform distribution

IOpts.Marginals(2).Name = 'RB1'; % bulk reaction rate coefficient  O3 + OL --> product + CI (RB1)
IOpts.Marginals(2).Type = 'Uniform';
IOpts.Marginals(2).Parameters = log10([1e-18 1e-13]); %for loguniform distribution

IOpts.Marginals(3).Name = 'dXY'; %diffusion coefficient of X in Y [cm2 s-1]
IOpts.Marginals(3).Type = 'Uniform';
IOpts.Marginals(3).Parameters = log10([1e-11 1e-5]); %for loguniform distribution

IOpts.Marginals(4).Name = 'dYY'; %self-diffusion coefficient of Y [cm2 s-1]
IOpts.Marginals(4).Type = 'Uniform';
IOpts.Marginals(4).Parameters = log10([1e-12 1e-6]); %for loguniform distribution

IOpts.Marginals(5).Name = 'HXY'; %Henry's law coefficient of X in Y []
IOpts.Marginals(5).Type = 'Uniform';
IOpts.Marginals(5).Parameters = log10([5e-6 5e-3]); %for loguniform distribution

IOpts.Marginals(6).Name = 'deX'; %desorption lifetime of X [s]
IOpts.Marginals(6).Type = 'Uniform';
IOpts.Marginals(6).Parameters = log10([1e-9 1e-2]); %for loguniform distribution

IOpts.Marginals(7).Name = 'acX'; %surface accommodation coefficient of X []
IOpts.Marginals(7).Type = 'Uniform';
IOpts.Marginals(7).Parameters = log10([1e-4 1]); %for loguniform distribution

IOpts.Marginals(8).Name = 'rad'; %outer particle radius [cm]
IOpts.Marginals(8).Type = 'Uniform';
IOpts.Marginals(8).Parameters = log10([2.5e-6 5e-3]); %for loguniform distribution

IOpts.Marginals(9).Name = 'gsX';%initial gas phase concentration of X [cm-3]
IOpts.Marginals(9).Type = 'Uniform';
IOpts.Marginals(9).Parameters = log10([1e11 5e15]); %for loguniform distribution

IOpts.Marginals(10).Name = 'gsY';%initial concentration of Y [cm-3]
IOpts.Marginals(10).Type = 'Uniform';
IOpts.Marginals(10).Parameters = log10([1e19 2e21]); %for loguniform distribution

% Create an INPUT object based on the specified marginals:
myInput = uq_createInput(IOpts);
%% load PCE for 50% lifetime
tic
myPCE_50 = load('PCE_results_20k_Bootstrap_50.mat','myPCE');
myPCE_50 = myPCE_50.myPCE;
toc
%% loop through values of ozone and calculate PCEs
n = 10; %number of grid samples

gsX_const = log10(logspace(log10(1e11), log10(5e15),n+2)); %range in ozone conc.
gsX_const = gsX_const(2:end-1);
rad_const = log10(logspace(log10(2.5e-6),log10(5e-3),n+2)); %range in radius.
rad_const = rad_const(2:end-1);

IoptsConst = IOpts; % copy the original input options object
%% initialize variables
Total_Sobol = zeros(length(gsX_const), length(rad_const), 10);
First_Sobol = zeros(length(gsX_const), length(rad_const), 10);
Second_Sobol = zeros(length(gsX_const), length(rad_const), 28);
LOO_error =  zeros(length(gsX_const), length(rad_const));

tic
for i =1 :length(gsX_const)
    %gsX
    IoptsConst.Marginals(9).Type = 'constant';
    IoptsConst.Marginals(9).Parameters = gsX_const(i); %for loguniform distribution
    
    for j= 1:length(rad_const)
        %rad
        IoptsConst.Marginals(8).Type = 'constant';
        IoptsConst.Marginals(8).Parameters = rad_const(j); %for loguniform distribution
        
        myInputConst = uq_createInput(IoptsConst);
        
        Xconst = uq_getSample(myInputConst, 1000); % draw samples from the new input object
        Yconst_10 = uq_evalModel(myPCE_50, Xconst); % evaluate your N-dim PCE
        
        % re-use MetaOpts from creation of myPCE: same settings
        MetaOpts.Input = myInputConst; % make sure the PCE uses the new input object
        MetaOpts.Type = 'Metamodel';
        MetaOpts.MetaType = 'PCE';
        
        MetaOpts.ExpDesign.X = Xconst;
        MetaOpts.ExpDesign.Y = Yconst_10;
        MetaOpts.Degree = 2:14;
        MetaOpts.TruncOptions.qNorm = 1;

        myPCEconst = uq_createModel(MetaOpts); % should have an LOO of ~1e-30 (exact fit) - does not!
        
        %sensitivity analysis
        YSobolOpts.Type = 'Sensitivity';
        YSobolOpts.Method = 'Sobol';

        % Set the maximum Sobol' index order to 2
        YSobolOpts.Sobol.Order = 2;

        % Create and add the sensitivity analysis to UQLab
        PCESobolAnalysis = uq_createAnalysis(YSobolOpts);
        
        %save total, first, and second order indices
        Total_Sobol(i,j,:) = PCESobolAnalysis.Results.Total;
        First_Sobol(i,j,:) = PCESobolAnalysis.Results.FirstOrder;
        Second_Sobol(i,j,:) = PCESobolAnalysis.Results.AllOrders{2};
        
        %save LOO error of PCE
        LOO_error(i,j) = myPCEconst.Error.ModifiedLOO;
    end
end
toc
%% Make plot of the Sobol' indices depending on [X] and radius
[OZ, RAD] = meshgrid(gsX_const, rad_const);

close all
figure('Position', [100 100 780 741])
VarNames = {'RS1','k_{BR}','D_{b,X}','dYY','H_{cp,X}','\tau_{d,X}','acX','gsY'};
vars_to_use = [2 3 5 6];
Total_Sobol_plot = Total_Sobol(:,:,vars_to_use);
for i = 1:4
    subplot(2,2,i)
    colormap(flipud(hot)) % oranges from Colorbrewer used in publication
    Total_temp = Total_Sobol_plot(:,:,i);
    scatter(10.^OZ(:), 10.^RAD(:), 100, Total_temp(:),'filled')
    xlabel('[X]_g (cm^{-3})')
    ylabel('r_p (cm)')
    caxis([0 0.4])
    set(gca,'Xscale','log')
    set(gca,'Yscale','log')
    set(gca,'Fontsize',18)
    title(VarNames{vars_to_use(i)})
    xticks([10^11 10^13 10^15])
    yticks([10^-5 10^-4 10^-3])
    xlim([1e11 5e15])
    ylim([2.5e-6 5e-3])
end
c=colorbar;
set(c, 'Location', 'southoutside')
xlabel(c,'Total Sobol index','Fontsize', 20);
set(c, 'Fontsize', 18)
set(c, 'Position', [0.038 .0664 .925 .02])
%% apply common random number sampling

%  get sample within the parameter space
IoptsConst.Marginals(8).Type = 'constant';
IoptsConst.Marginals(8).Parameters = rad_const(end); %for loguniform distribution

IoptsConst.Marginals(9).Type = 'constant';
IoptsConst.Marginals(9).Parameters = gsX_const(end); %for loguniform distribution
myInputConst = uq_createInput(IoptsConst);

X_resample_upright = uq_getSample(myInputConst,10000,'LHS') ;

IoptsConst.Marginals(8).Type = 'constant';
IoptsConst.Marginals(8).Parameters = rad_const(1); %for loguniform distribution

IoptsConst.Marginals(9).Type = 'constant';
IoptsConst.Marginals(9).Parameters = gsX_const(1); %for loguniform distribution
myInputConst = uq_createInput(IoptsConst);

X_resample_downleft = uq_getSample(myInputConst,10000,'LHS') ;


%% sample variables at different points
vars_to_test = [6] ; % deX
lower_bounds = [log10(1e-9)];
upper_bounds = [ log10(1e-2)];

n_plot = 20;
close all
figure('Position', [100 100 400 741]);
Varnames_used = {'\tau_{d,X}'};
units = {'s'};

subplot(2,1,1)
Variable_used = vars_to_test(i);
abcissa_values = linspace(lower_bounds(i), upper_bounds(i),n_plot);%points at which to test variable

for j = 1:n_plot
    temp = X_resample_upright; %temp variable for X values
    temp(:,Variable_used) = abcissa_values(j);%replace column with abcissa value
    Y_resample = uq_evalModel(myPCE_50,temp) ; %calculate PCE
    Y_resample_mean(j)  =mean(Y_resample); %mean lifetime
    Y_resample_std(j)  =std(Y_resample); %standard deviation lifetime
end
errorbar(10.^(abcissa_values),Y_resample_mean,Y_resample_std,'-k','vertical','Linewidth', 2,'Capsize', 3)
set(gca, 'Xscale', 'log')
hold on
semilogx(10.^(abcissa_values),Y_resample_mean,'-k','Linewidth', 3,'HandleVisibility','off')
if i==1
    legend("Mean response \pm 1\sigma")
end
xlabel(Varnames_used{i} + " (" + units{i} + ")",'Fontsize', 18);
ylabel('log(50% lifetime)','Fontsize', 18);
set(gca,'Fontsize',17)
title("[X]_g = 1.87e15 cm^{-3}, r_p = 2.5e-3 cm", 'Fontsize',20)
axis tight
ylim([0.8 6])


subplot(2,1,2)
Variable_used = vars_to_test(i);
abcissa_values = linspace(lower_bounds(i), upper_bounds(i),n_plot);%points at which to test variable

for j = 1:n_plot
    temp = X_resample_downleft; %temp variable for X values
    temp(:,Variable_used) = abcissa_values(j);%replace column with abcissa value
    Y_resample = uq_evalModel(myPCE_50,temp) ; %calculate PCE
    Y_resample_mean(j)  =mean(Y_resample); %mean lifetime
    Y_resample_std(j)  =std(Y_resample); %standard deviation lifetime
end
errorbar(10.^(abcissa_values),Y_resample_mean,Y_resample_std,'-k','vertical','Linewidth', 2,'Capsize', 3,'HandleVisibility','off')
set(gca, 'Xscale', 'log')
hold on
semilogx(10.^(abcissa_values),Y_resample_mean,'-k','Linewidth', 3,'HandleVisibility','off')
xlabel(Varnames_used{i} + " (" + units{i} + ")",'Fontsize', 18);
ylabel('log(50% lifetime)','Fontsize', 18);
set(gca,'Fontsize',17)
title("[X]_g = 2.67e11 cm^{-3}, r_p = 5.0e-6 cm", 'Fontsize',20)
axis tight
ylim([0.8 6])
