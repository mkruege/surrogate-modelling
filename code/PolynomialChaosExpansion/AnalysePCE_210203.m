%% Introduction
% This is a script to import generated PCEs, load the validation data,
% generate an output from the PCEs and compare with validation data.

%% Initiation
clearvars

Size_Samples=[50 100 200 500 1000 2000 5000 10000 20000];
Names_Var=["Yval", "Nexp50", "Nexp100", "Nexp200", "Nexp500", "Nexp1000", "Nexp2000", "Nexp5000", "Nexp10000", "Nexp20000"];

addpath(genpath('Path')) % Use this to add input data path
uqlab % Starts uqlab

%% Retrieve data sets
% Start reading the data file
Input_Validation = load(fullfile('InputTable_Validation_90_200811.mat'));
Data_Validation=Input_Validation.Input;

% Store the content of the validation data file in two matrices:
Xval = log10(Data_Validation(:, 1:10));
Yval = log10(Data_Validation(:, 11));

IO=zeros(length(Yval(:,1)), length(Size_Samples)+1); % Input and output
IO(:,1)=Yval;

for i=1:length(Size_Samples)
    % Load PCE file
    load(sprintf(...
        'OLPCEeuler_90_Nexp%i.mat',...
        Size_Samples(i)))
    
    % Evaluate the PCE metamodel at the validation set points
    YPCE=uq_evalModel(myPCE, Xval);
    IO(:,i+1)=YPCE;
end

IO=array2table(IO, 'VariableNames', Names_Var);

writetable(IO, 'PCE_valdiation_output_90_210203.csv')