CODE FOR NEURAL NETWORK SURROGATE MODEL GENERATION AND APPLICATION  
Matteo Krüger (m.krueger@mpic.de)  
Multiphase Chemistry Department  
Max Planck Institute for Chemistry, Mainz  
Last update: 2022/09/07  
  
exec_surr_modeling.py - python executable for hyperparameter tuning and model training  
fit_acquisition_test.py - pre-sampling with NN for NN-suggested fit acquisition  
Matlab_Sampling.py - execution of matlab model scripts with matlab engine  
MCMC.py - functions for pre-sampling with NN (Metropolis Hastings python implementation and random loguniform batch sampling), error calculation  
mogon_hpt_iterator.py - hyperparameter tuning, saving of model results and best model pickles  
mogon_model.py - NN model with variable architecture, data pre-processing  
PredictFits.py - supporting functions for application of pickled NN models (predictions)  
ReadData.py - supporting function for specific data structure and data file handling
