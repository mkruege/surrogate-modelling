import mogon_model
import pandas as pd
import pickle
import csv

def run_hpt(hpt_file, size, cross_val):
    "Function to train <cross_val> NN models for each row in the provided hpt_file on the specified training data size. hpt_file is a csv with one column for each parameter in 'params'. Training and test data file names are hard-coded in mogon_model.py. MSEs, pickles and scalers of best models are saved."
    hpt = pd.read_csv(hpt_file)

    for_hpt_csv = []
    best_mse = 1000000

    for i in range(hpt.shape[0]):
	
	# reading hyperparameters from file provided
        params = {'model_id': [hpt["model_id"][i]],
                  'hl': [hpt["hl"][i]],
                  'activation1': [hpt["a1"][i]],
                  'activation2': [hpt["a2"][i]],
                  'activation3': [hpt["a3"][i]],
                  'activation4': [hpt["a4"][i]],
                  'activation5': [hpt["a5"][i]],
                  'first_hidden_layer': [hpt["n1"][i]],
                  'second_hidden_layer': [hpt["n2"][i]],
                  'third_hidden_layer': [hpt["n3"][i]],
                  'fourth_hidden_layer': [hpt["n4"][i]],
                  'fifth_hidden_layer': [hpt["n5"][i]],
                  'first_dropout': [hpt["do1"][i]],
                  'second_dropout': [hpt["do2"][i]],
                  'third_dropout': [hpt["do3"][i]],
                  'fourth_dropout': [hpt["do4"][i]],
                  'fifth_dropout': [hpt["do5"][i]],
                  'batch_size': [hpt["batch_size"][i]],
                  'epochs': [hpt["epochs"][i]],
                  'inputs': [10],
                  'outputs': [3],
                  'lr': [hpt["lr"][i]],
                  'decay': [hpt["decay"][i]]}
	
	# no cross-validation
        if cross_val == 0:
            [model, [scaler_x, scaler_y], mse, Y_test, Y_pred, trainingtime] = mogon_model.run_model(params, size, cross_val)

            # do always:
            #save data for hpt sheet
            for_hpt_csv.append([params['model_id'], mse, trainingtime])
            Y_test = 10**Y_test
            Y_pred = 10**Y_pred
	    
	    # save MSE of current model in csv
            with open("MSEs_" + str(size) + ".csv", "w") as csv_file:
                writer = csv.writer(csv_file, delimiter=",")
                writer.writerow(["model_id", "mse", "trainingtime"])
                for row in for_hpt_csv:
                    writer.writerow(row)


            # do only if model is currently best:
            if mse < best_mse:
                # pickle model and x_y_scalers
                pickle.dump([scaler_x, scaler_y], open("scaler_x_y_" + str(size) + ".sav", 'wb'))
                model.save("best_model_" + str(size) + ".h5")
                best_mse = mse
                best_preds_A1 = Y_pred[:,0]
                best_preds_A2 = Y_pred[:,1]
                best_preds_A3 = Y_pred[:,2]
	
	# with cross-validation
        else:
            returns = mogon_model.run_model(params, size, cross_val)
            mses = [x[2] for x in returns]
            models = [x[0] for x in returns]
            Y_test = returns[0][3]
            Y_preds = [x[4] for x in returns]
            trainingtimes = [x[5] for x in returns]
            scaler_x = returns[0][1][0]
            scaler_y = returns[0][1][1]
            mse = sum(mses)/len(mses)
            trainingtime = sum(trainingtimes)/len(trainingtimes)

            # do always:
            # save data for hpt sheet
            for_hpt_csv.append([params['model_id'], mse, trainingtime])
            for m in mses:
                for_hpt_csv[-1].append(m)
            Y_test = 10 ** Y_test
            Y_pred = [10 ** x for x in Y_preds]

            header2 = ["model_id", "mse", "trainingtime"]
            for mi, m in enumerate(mses):
                header2.append("mse_model" + str(mi))

            with open("MSEs_" + str(size) + hpt_file[:-4] + ".csv", "w") as csv_file:
                writer = csv.writer(csv_file, delimiter=",")
                writer.writerow(header2)
                for row in for_hpt_csv:
                    writer.writerow(row)

            # do onl if model is currently best:
            if mse < best_mse:
                # pickle model and x_y_scalers
                pickle.dump([scaler_x, scaler_y], open("scaler_x_y_" + str(size) + ".sav", 'wb'))
                for modnum, model in enumerate(models) :
                    model.save("best_model_" + str(size) + "_" + str(modnum) + ".h5")
                    best_mse = mse
                    best_best_mse = min(mses)
                    best_best_mse_index = mses.index(min(mses))
                    best_preds_A1 = Y_pred[best_best_mse_index][:, 0]
                    best_preds_A2 = Y_pred[best_best_mse_index][:, 1]
                    best_preds_A3 = Y_pred[best_best_mse_index][:, 2]

    # save predictions for provided test set
    try:
        df = pd.read_csv("predictions.csv")
        df["A1_pred_" + str(size)] = best_preds_A1
        df["A2_pred_" + str(size)] = best_preds_A2
        df["A3_pred_" + str(size)] = best_preds_A3
        df.to_csv("predictions.csv", index=False)
    except:
        header = ["Y_A1", "Y_A2", "Y_A3"]
        with open("predictions.csv", "w") as csv_file:
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(header)
            for row in Y_test:
                writer.writerow(row)

        df = pd.read_csv("predictions.csv")
        df["A1_pred_" + str(size)] = best_preds_A1
        df["A2_pred_" + str(size)] = best_preds_A2
        df["A3_pred_" + str(size)] = best_preds_A3
        df.to_csv("predictions.csv", index=False)

    return

