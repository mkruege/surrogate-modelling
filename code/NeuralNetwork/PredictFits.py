import numpy as np
from matplotlib import pyplot as plt
from joblib import load
import pandas as pd
from sklearn.model_selection import train_test_split
import random





def pickle_test_predictions_reverse(pickle, filename, labdata, featurepara):
    "Makes reversed prediction for parameters or one parameter specified in featurepara based on trained model (pickle) and lab dataset. CSV-File specified is not important, only for overall data structure"

    #read data and return dataframe
    labels = pd.read_csv(filename)
    split = 2

    # seperate labels and features
    if featurepara == 0:
        featurenames = []
        featureList = []
        for i in range(100):
            featurenames.append("A" + str(i))
        for j in range(100):
            featurenames.append("B" + str(j))
        for i in featurenames:
            featureList.append(labels[i])
            labels = labels.drop(i, axis=1)
        featurest = np.array(featureList)
        features = np.transpose(featurest)
        labels_list = list(labels.columns)
        labels = np.array(labels)

    else:
        featurenames = []
        featureList = []
        otherparas = []
        for i in labels:
            if i not in featurepara:
                if i not in "errA":
                    if i not in "errB":
                        if i[0] in "P":
                            otherparas.append(i)
                        else:
                            featurenames.append(i)
            else:
                if len(featurepara) != len(i): #remove P1 for P10, P11 ...
                    otherparas.append(i)

        for i in featurenames:
            featureList.append(labels[i])
            labels = labels.drop(i, axis=1)
        for i in otherparas:
            labels = labels.drop(i, axis=1)
        featurest = np.array(featureList)
        features = np.transpose(featurest)
        labels_list = list(labels.columns)
        labels = np.array(labels)

    # split training and test dataset
    train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size=split,
                                                                                random_state=random.randint(1, 100))
    errorlist = []
    errorlist.append(test_labels[:, -2])
    errorlist.append(test_labels[:, -1])
    test_labels = test_labels[:, :-2]
    train_labels = train_labels[:, :-2]


    new_paras = np.vstack([test_features, labdata])

    # call algorithm
    rf = load(pickle)

    # Use the forest's predict method on the test data
    predictions = rf.predict(new_paras)

    return predictions[2]


def predict_any_fit_with_pickle(pickle, filename, lambdaa, output, resultA=0, resultB=0):

    if output == "A":
        lambdaa = np.asarray(lambdaa)


        predictions = pickle_test_predictions(pickle, filename, lambdaa)
        if resultA != 0:
            plt.plot(resultA, color="r", label="DIFEQ model result A")
        plt.plot(predictions[10][0:100], label=pickle)
        plt.legend()
        plt.title("Predictions for best-fit parameter set. Model " + pickle)
        plt.show()

    if output == "B":
        lambdaa = np.asarray(lambdaa)


        predictions = pickle_test_predictions(pickle, filename, lambdaa)
        if resultB != 0:
            plt.plot(resultB, color="r", label="DIFEQ model result B")
        plt.plot(predictions[10][100:200], label=pickle)
        plt.legend()
        plt.title("Predictions for best-fit parameter set. Model " + pickle)
        plt.show()

def pickle_test_predictions(pickle, filename, parameters):
    "Makes a prediction based on a trained model (pickle) and a set of parameters. CSV-File specified is not important, only for overall data structure"
    # call function to read data and return dataframe
    features = pd.read_csv(filename)

    # calculate test dataset split size
    rows = features.shape[0]
    split = 10

    # seperate labels and features
    labelnames = []
    labelsList = []
    for i in range(100):
        labelnames.append("A" + str(i))
    for j in range(100):
        labelnames.append("B" + str(j))
    labelnames.append("errA")
    labelnames.append("errB")
    for i in labelnames:
        labelsList.append(features[i])
        features = features.drop(i, axis=1)
    labelst = np.array(labelsList)
    labels = np.transpose(labelst)
    feature_list = list(features.columns)
    features = np.array(features)

    # split training and test dataset
    train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size=split,
                                                                                random_state=random.randint(1, 100))

    new_paras = np.vstack([test_features, parameters])

    # call algorithm
    rf = load(pickle)

    # Use the model's predict method on the test data
    predictions = rf.predict(new_paras)

    return predictions

