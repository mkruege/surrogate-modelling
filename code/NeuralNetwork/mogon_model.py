from __future__ import absolute_import, division, print_function, unicode_literals
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
import ReadData
from sklearn.ensemble import RandomForestRegressor
import random
import pickle
from sklearn.metrics import mean_squared_error
import time
from tensorflow.keras.optimizers import Adam
from sklearn.model_selection import KFold



def run_model(params, samplesize, cross_val, inputs=10, outputs=3, maxval=0, logoutput=1, rforest=0):
    "Model trainings for individual NN-architecture, specified by dict 'params'. 
    trainfilename = "P0sortTrain.csv"
    testfilename = "P0sortTest.csv"

    # call function to read data and return dataframe
    trainfeatures = ReadData.readSortedCSV(trainfilename)
    testfeatures = ReadData.readSortedCSV(testfilename)

    if (trainfeatures.shape[0]-samplesize) > 150000:
        rforest=0

    if 'fifth_hidden_layer' in params:
        hl = 5
    elif 'fourth_hidden_layer' in params:
        hl = 4
    elif 'third_hidden_layer' in params:
        hl = 3
    elif 'second_hidden_layer' in params:
        hl = 2
    else:
        hl = 1

    size = (trainfeatures.shape[0] - samplesize)
    hptfilename = "HPt" + str(size) + "samples" + str(hl) + "hl.csv"

    # set input layer size to parNum
    params['inputs'] = [inputs]
    params['outputs'] = [outputs]

    # adapt pandas output for better hpt overview
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    # remove nans
    trainfeatures = trainfeatures.dropna(axis=0, how="any")
    testfeatures = testfeatures.dropna(axis=0, how="any")

    # shuffle dataset
    # from sklearn.utils import shuffle
    # features = shuffle(features)

    featurenames = []
    for i in range(outputs):
        featurenames.append(str("A" + str(i)))

    dropit = []
    if maxval != 0:
        for num, row in enumerate(trainfeatures['A0']):
            if row > maxval:
                dropit.append(num)
    dropit2 = []
    if maxval != 0:
        for num, row in enumerate(testfeatures['A2']):
            if row > maxval:
                dropit2.append(num)

    if logoutput == 1:
        for i in range(outputs):
            trainfeatures['A' + str(i)] = np.log10(trainfeatures['A' + str(i)])

        trainfeatures = trainfeatures.drop(trainfeatures.index[dropit], axis=0)

        for i in range(outputs):
            testfeatures['A' + str(i)] = np.log10(testfeatures['A' + str(i)])

        testfeatures = testfeatures.drop(testfeatures.index[dropit], axis=0)

    # calculate test dataset split size
    # rows = trainfeatures.shape[0]
    # split = samplesize / rows
    # if split < 0 or split > 1:
    #     print("Samplesize is larger than number of samples within maxvalue. Proceed with " + str(rows) + " samples.")
    #     split = rows

    # seperate labels and features
    labelnames = []
    trainlabelsList = []
    testlabelsList = []
    # set labelnames
    for i in range(outputs):
        labelnames.append("A" + str(i))
    # labelnames.append("A1")

    # append error as label if ought to be predicted
    labelnames.append("err")

    # add data to labels and remove from features
    for i in labelnames:
        if "err" not in i:
            trainlabelsList.append(trainfeatures[i])
            testlabelsList.append(testfeatures[i])
        trainfeatures = trainfeatures.drop(i, axis=1)  # remove from features
        testfeatures = testfeatures.drop(i, axis=1)  # remove from features

        # exclude 3 outputs
    if outputs == 3:
        for i in range(outputs):
            trainfeatures = trainfeatures.drop("A" + str(3 + i), axis=1)
            testfeatures = testfeatures.drop("A" + str(3 + i), axis=1)

        # exclude 2 outputs
        # trainfeatures = trainfeatures.drop("A0", axis=1)
        # trainfeatures = trainfeatures.drop("A2", axis=1)
        # testfeatures = testfeatures.drop("A0", axis=1)
        # testfeatures = testfeatures.drop("A2", axis=1)


    # transform data to arrays
    trainlabelst = np.array(trainlabelsList)
    testlabelst = np.array(testlabelsList)
    trainlabels = np.transpose(trainlabelst)
    testlabels = np.transpose(testlabelst)
    trainfeature_list = list(trainfeatures.columns)
    testfeature_list = list(testfeatures.columns)
    trainfeatures = np.array(trainfeatures)
    testfeatures = np.array(testfeatures)

    alllabels = np.vstack([trainlabels, testlabels])
    allfeatures = np.vstack([trainfeatures, testfeatures])

    alllabelst = np.transpose(alllabels)

    # get maximum experiment time and add to labels for normalization
    if 0: #old version
        maxtime = max(mx for mx in alllabelst[0])
        mintime = min(mn for mn in alllabelst[0])
    if 1: #corrected
        maxtime = np.max(alllabelst)
        mintime = np.min(alllabelst)

    print("Maximum value is: " + str(maxtime))
    print("Minimum value is: " + str(mintime))
    alllabels = np.vstack([alllabels, [maxtime for x in range(outputs)]])
    alllabels = np.vstack([alllabels, [mintime for x in range(outputs)]])

    allfeatures = np.log10(allfeatures)

    # normalize all features and labels
    scaler_y = MinMaxScaler()
    scaler_x = MinMaxScaler()

    print(scaler_x.fit(allfeatures))
    X = scaler_x.transform(allfeatures)
    print(scaler_y.fit(alllabels))
    Y = scaler_y.transform(alllabels)

    yscale = Y[:-1002]  # y train
    xscale = X[:-1000]  # x train
    X_test = X[-1000:]  # x test
    Y_test = Y[-1002:-2]  # y test


    try:
        dump1, X_sect, dump2, Y_sect = train_test_split(xscale, yscale, test_size=samplesize)
    except:
        print("WARNING! Number of Samples too low for input Test split - continues with hptsamples = 2 (Only DEBUG mode). Set test split size with >hptsample=n<")
        dump1, X_sect, dump2, Y_sect = train_test_split(xscale, yscale, test_size=2)

    # no cross-validation
    if cross_val == 0:
        try:
            X_train, X_val, Y_train, Y_val = train_test_split(X_sect, Y_sect, test_size=0.2)
        except:
            print("WARNING! Number of Samples too low for input Test split - continues with hptsamples = 2 (Only DEBUG mode). Set test split size with >hptsample=n<")
            X_train, X_val, Y_train, Y_val = train_test_split(xscale, yscale, test_size=2)

        if rforest == 0:
            model = olec_model(X_train, Y_train, X_test, Y_test, params)
            # keras.utils.plot_model(model, show_shapes=True)

            if params['decay'][0] != 0:
                model.compile(loss="mse", optimizer=Adam(learning_rate=params["lr"][0], decay=params['decay'][0]))
            else:
                model.compile(loss="mse", optimizer=Adam(learning_rate=params["lr"][0]))
            # model.summary()

            time1 = time.time()
            model = model.fit(X_train, Y_train, batch_size=params['batch_size'][0], epochs=params['epochs'][0],
                              verbose=1, validation_data=(X_val, Y_val)).model
            time2 = time.time()

            trainingtime = time2 - time1

        # Random Forest for comparison
        if rforest == 1:
            model = RandomForestRegressor(n_estimators=100, max_depth=None, min_samples_split=4,
                                          min_samples_leaf=2, max_features="auto",
                                          random_state=random.randint(1, 100))

            time1 = time.time()
            # Train the model on training data
            model.fit(X_sect, Y_sect);
            time2 = time.time()

            trainingtime = time2 - time1

        Y_pred = model.predict(X_test)

        Y_pred = MinMaxScaler.inverse_transform(scaler_y, Y_pred)

        Y_test = MinMaxScaler.inverse_transform(scaler_y, Y_test)

        mse = mean_squared_error(Y_test, Y_pred)

        return [model, [scaler_x, scaler_y], mse, Y_test, Y_pred, trainingtime]

    # with cross-validation - no implementation for RM
    else:
        returns = []
        kf = KFold(n_splits=cross_val)
        kf.get_n_splits(X_sect)
        KFold(n_splits=cross_val, random_state=None, shuffle=False)
        sets = []
        for train_index, test_index in kf.split(X_sect):
            X_train, X_val = X_sect[train_index], X_sect[test_index]
            Y_train, Y_val = Y_sect[train_index], Y_sect[test_index]
            sets.append([X_train, X_val, Y_train, Y_val])

        for setnum, set in enumerate(sets):
            [X_train, X_val, Y_train, Y_val] = set

            if rforest == 0:
                model = olec_model(X_train, Y_train, X_val, Y_val, params)
                # keras.utils.plot_model(model, show_shapes=True)

                if params['decay'][0] != 0:
                    model.compile(loss="mse", optimizer=Adam(learning_rate=params["lr"][0], decay=params['decay'][0]))
                else:
                    model.compile(loss="mse", optimizer=Adam(learning_rate=params["lr"][0]))
                # model.summary()

                time1 = time.time()
                model = model.fit(X_train, Y_train, batch_size=params['batch_size'][0], epochs=params['epochs'][0],
                                  verbose=1, validation_data=(X_val, Y_val)).model
                time2 = time.time()

                trainingtime = time2 - time1

            else:
                print("Please run Random Forest without Cross Validation (cross_val = 0).")

            Y_pred = model.predict(X_test)

            Y_pred = MinMaxScaler.inverse_transform(scaler_y, Y_pred)

            Y_test_curr = MinMaxScaler.inverse_transform(scaler_y, Y_test)

            mse = mean_squared_error(Y_test_curr, Y_pred)

            returns.append([model, [scaler_x, scaler_y], mse, Y_test_curr, Y_pred, trainingtime])
        return returns


def olec_model(X_train, y_train, x_val, y_val, params): #simple 1-5 hidden layer NN
    model = Sequential()
    model.add(Dense(params['first_hidden_layer'][0], input_shape=[params['inputs'][0]], activation=params['activation1'][0], use_bias=True))
    model.add(Dropout(params['first_dropout'][0]))

    if params["hl"][0] > 1:
        model.add(Dense(params['second_hidden_layer'][0],
                         activation=params['activation2'][0],
                         use_bias=True))
        model.add(Dropout(params['second_dropout'][0]))

    if params["hl"][0] > 2:
        model.add(Dense(params['third_hidden_layer'][0],
                 activation=params['activation3'][0],
                 use_bias=True))
        model.add(Dropout(params['third_dropout'][0]))

    if params["hl"][0] > 3:
        model.add(Dense(params['fourth_hidden_layer'][0],
                    activation=params['activation4'][0],
                    use_bias=True))
        model.add(Dropout(params['fourth_dropout'][0]))

    if params["hl"][0] > 4:
        model.add(Dense(params['fifth_hidden_layer'][0],
                    activation=params['activation5'][0],
                    use_bias=True))
        model.add(Dropout(params['fifth_dropout'][0]))


    model.add(Dense(params['outputs'][0], activation="linear"))

    return model
