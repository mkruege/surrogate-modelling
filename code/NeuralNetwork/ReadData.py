import pandas as pd
import csv
import os

# supporting functions for data pre-processing and data set handling

def handle_new_data(parametersetname, runs, param, ds, linlen, fpar, outputs):
    "converts new input sample data sheets to sorted and merged sample data sheets"

    #######Sort raw CSVs############
    eval("convertRawCSV('" + parametersetname + ".csv', '" + parametersetname + "sort.csv', 1, param, ds, linlen, fpar, outputs)")

    return


def handle_renamed_files(filenum, param, ds, linlen, fpar, outputs):
    #sort raw CSVs
    fails = -1
    for i in range(filenum):
        try:
            eval(
                "convertRawCSV('P0 (" + str(i - fails) + ").csv', 'P" + str(i - fails - 1) + "sort.csv', 1, param, ds, linlen, fpar, outputs)")
            print("File " + str(i) + " of " + str(filenum) + " Files sorted.")
        except:
            fails = fails+1
            print("File " + str(i) + " could not be sorted. Continue...")

    print("All files sorted.")
    filesleft = True
    filenum=-10
    while filesleft==True:
        filenum=filenum+10
        for i in range(9):
            try:
                mergeSortedCSV("P" + str(filenum), "P" + str(filenum + i + 1), "P" + str(filenum))
            except:
                filesleft=False

        if filenum != 0:
            try:
                mergeSortedCSV("P0", "P" + str(filenum), "P0")
            except:
                pass
        print(str(filenum) + " files merged in P0.")

    return



def convertRawCSV(filename, newfilename, witherr, param, ds, linlen, fpar, outputs):
    "Reads one-row csv file and saves as sorted"
    if witherr == 0:
        countuntil = linlen
    if witherr == 1:
        countuntil = linlen
    dataset = pd.read_csv(filename, header = None)
    raw = dataset.to_numpy()
    sorted = []
    for line in raw:
        newline = []
        counter = 0
        for value in line:
            if counter < countuntil: #length of one row in sorted file
                newline.append(value)
                counter = counter + 1
            if counter == countuntil:
                sorted.append(newline)
                newline = []
                counter = 0

    header = []
    for l in range(param):
        header.append("P" + str(l))
    if ds != 1:
        for i in range(fpar):
            header.append("F" + str(i))
    for i in range(outputs): #rest of header - output1
        header.append("A" + str(i))

    if witherr == 1:
        header.append("err")

    with open(newfilename, "w+") as csv_file: #create file and write header
        writer = csv.writer(csv_file, delimiter = ",")
        writer.writerow(header)

    for row in sorted:
        with open(newfilename, "a") as csv_file: #write rows
            writer = csv.writer(csv_file, delimiter = ",")
            writer.writerow(row)

    return


def readSortedCSV(filename): #reads sorted csv and returns pandas-dataframe
    "Reads csv file and returns data in pandas.dataframe"
    dataset = pd.read_csv(filename)
    return dataset

def mergeSortedCSV(filename1, filename2, newfilename): #merge two sorted CSV files
    "Merges sorted csv files"
    filename1 = filename1 + "sort.csv"
    filename2 = filename2 + "sort.csv"
    newfilename = newfilename + "sort.csv"
    dataset1 = pd.read_csv(filename1)
    dataset2 = pd.read_csv(filename2)
    df1 = pd.DataFrame(dataset1)
    df2 = pd.DataFrame(dataset2)
    whole = pd.concat([df1, df2])
    whole.to_csv(newfilename, index=False)
    return

def mergeSortedCSVnE(filename1, filename2, newfilename): #merge two sorted CSV files
    "Merges sorted csv files"
    filename1 = filename1 + "sort.csv"
    filename2 = filename2 + ".csv"
    newfilename = newfilename + "sort.csv"
    dataset1 = pd.read_csv(filename1)
    dataset2 = pd.read_csv(filename2)
    df1 = pd.DataFrame(dataset1)
    df2 = pd.DataFrame(dataset2)
    whole = pd.concat([df1, df2])
    whole.to_csv(newfilename, index=False)
    return

def cutSortedCSV(filename, samples, newfilename, newfilename2, removenans=1):
    "Cuts csv files into certain samplesize"
    dataset = pd.read_csv(filename)
    # remove nans
    if removenans==1:
        dataset = dataset.dropna(axis=0, how="any")
    delete = dataset.size - samples
    new = dataset.iloc[0:samples]
    new2 = dataset.iloc[samples:]
    new.to_csv(newfilename, index=False)
    new2.to_csv(newfilename2, index=False)
    return

def removeCSV(filename):
    "Removes csv file from folder"
    filename = filename + ".csv"
    os.remove(filename)