import mogon_hpt_iterator
from sys import argv
import argparse

model_dir = './'
if argv[0].find('/') >= 0:
	model_dir = argv[0][: - argv[0][::-1].find('/')]


#parse arguments
parser = argparse.ArgumentParser(description='Launching iteration over HPT sheet for specific training set size.')
parser.add_argument('--hpt_sheet', '-hp', type=str, default='hpt_sheet.csv', help='Path to bed file with hpt information')
parser.add_argument('--size', '-s', type=int, default=0, help='Size of training set.')
parser.add_argument('--cross_val', '-cv', type=int, default=5, help='Define k for k-fold cross validation.')


# parse and pre-process command line arguments
args = parser.parse_args()

print("Arguments parsed")

# execute NN hyperparameter tuning
mogon_hpt_iterator.run_hpt(args.hpt_sheet, args.size, args.cross_val)