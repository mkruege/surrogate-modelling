import MCMC
import time
import pandas as pd
from timeout import timeout
import numpy as np
from matplotlib.colors import LogNorm
import seaborn as sns
from matplotlib import pyplot as plt
import pandas as pd
from cycler import cycler

def test_loguniform_batch_sampling(model, scaler, batchsize, desiredfits, cmprdata_out, cmprdata_in, rRMSE_lim=2, verbose=1):
    "Performs loguniform batch sampling with provided NN model pickle."
    time1 = time.time()
    [testedsamples, nfits] = MCMC.perform_loguniform_sampling(model, scaler, desiredfits, batchsize, cmprdata_out=cmprdata_out, cmprdata_in=cmprdata_in, rRMSE_lim=rRMSE_lim, verbose=verbose)
    time2 = time.time()
    sampletime = time2 - time1
    print("#############NN LOGUNIFORM sampling###############")
    print("rRMSE threshold: " + str(rRMSE_lim))
    print("Tested samples: " + str(testedsamples))
    print("Time required [s]: " + str(sampletime))
    print("Fits: " + str(nfits))
    print("Acceptance rate: " + str((nfits/testedsamples) * 100) + " %")
    print("######################################")
    return

def test_MCMC_sampling(model, scaler, desiredfits, cmprdata_out, cmprdata_in, rRMSE_lim=2, batchsize=1, verbose=1):
    "Performs MCMC sampling with provided NN model pickle."
    time1 = time.time()
    [testedsamples, nfits] = MCMC.perform_MCMC(0.1, 500000000, model, scaler, rRMSE_lim, [cmprdata_in, cmprdata_out], desiredfits, batchsize=batchsize)
    time2 = time.time()
    sampletime = time2 - time1
    print("#############NN Metropolis Hastings sampling###############")
    print("rRMSE threshold: " + str(rRMSE_lim))
    print("Tested samples: " + str(testedsamples))
    print("Time required [s]: " + str(sampletime))
    print("Fits: " + str(nfits))
    print("Acceptance rate: " + str((nfits / testedsamples) * 100) + " %")
    print("################################################")
    return
