import matlab.engine
import time

# functions to call kinetic models via matlab engine

def get_KM3_sample(params):
    try:
        eng = matlab.engine.start_matlab()
    except:
        input("Matlab engine not successfully launched - make sure VPN is connected and press Enter to continue...")
        eng = matlab.engine.start_matlab()

    #print("Matlab engine successfully launched.")
    #print("#######################MATLAB#########################")


    a = eng.ComputeData(params)

    #print("Experiment successfully simulated with KM3 - closing Matlab engine.")
    #print("######################################################")

    return [x for x in a[0]]


def get_KM_SUB_multisample(params, desiredruns, cmprdata_out, th, kmsub=1):
    import pandas as pd
    import MCMC
    import numpy as np

    try:
        eng = matlab.engine.start_matlab()
    except:
        input("Matlab engine not successfully launched - make sure VPN is connected and press Enter to continue...")
        eng = matlab.engine.start_matlab()

    print("Matlab engine successfully launched.")
    print("#######################MATLAB#########################")

    params_l = params.values.tolist()

    errors = []
    fits = 0
    times = []
    for i in range(desiredruns):
        time1 = time.time()
        if kmsub == 1:  # for first manuscript
            #try:
            o = eng.run_NN_suggested(params_l[i])
            out = np.asarray(o).reshape((6, 3))
            error = MCMC.calc_error_6sets(out, cmprdata_out, 3)
            #except:
            #    break
        else: #km3 for second manuscript
            o = eng.run_NN_suggested_km3(params_l[i])
            out = np.asarray(o).reshape((6, 9))
            error = MCMC.calc_error_6sets(out, cmprdata_out, 9)

        print(out)



        print(error)
        time2 = time.time()
        errors.append(error)
        times.append(time2 - time1)
        if error < th:
            fits = fits+1


    print("Experiment successfully simulated with KM3 - closing Matlab engine.")
    print("######################################################")

    return [errors, times, fits]

def get_KM_SUB_sample(params):
    import pandas as pd
    import MCMC
    import numpy as np

    try:
        eng = matlab.engine.start_matlab()
    except:
        input("Matlab engine not successfully launched - make sure VPN is connected and press Enter to continue...")
        eng = matlab.engine.start_matlab()

    #print("Matlab engine successfully launched.")
    #print("#######################MATLAB#########################")
    try:
        params_l = params.values.tolist()
    except:
        try:
            params_l = [x[0] for x in params.tolist()]
        except:
            params_l = params.tolist()

    o = eng.run_NN_suggested(params_l)

    out = np.asarray(o).reshape((6, 3))

    #print("Experiment successfully simulated with KM3 - closing Matlab engine.")
    #print("######################################################")

    return out