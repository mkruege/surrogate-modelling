from tensorflow import keras
import numpy as np
import seaborn as sns
import pickle
import random
import pandas as pd
import csv
import scipy
import matplotlib.pyplot as plt
import Matlab_Sampling

# python implementations for MCMC and random loguniform batch sampling (used for pre-sampling with the NN surrogate model, but can be used with matlab engine for KM)
# MCMC code adapted from https://github.com/jclemoyne/BayesianEngine/blob/master/MCMC.py

def scan_csv_with_new_th(input_file, rRMSEth, data, model, scaler, outputs=9):

    if not "KM-SUB" in model:
        # LOAD NN FORWARD MODEL (3 outputs)
        NNmodel = keras.models.load_model(model)

        # LOAD x- and y-scaler associated to this model
        [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))


    cmprdata = data[0]
    data_out = data[1]

    fits = pd.read_csv(input_file)
    fits = fits.dropna()
    fits = fits.reset_index(drop=True)
    if "rRMSE" in fits.columns:
        filtered_fits = fits[fits['rRMSE'] < rRMSEth]
    else:
        try:
            only_fits = fits.drop(["out1", "out2", "out3", "out4", "out5", "out6", "out7", "out8", "out9"], axis=1)
        except:
            only_fits = fits
        rRMSEs = []
        for rown, row in enumerate(only_fits.iterrows()):
            print("Step " + str(rown) + " of " + str(only_fits.shape[0]))
            x = list(row[1])
            if not "KM-SUB" in model:
                preds = nn_model_pred([np.transpose(np.append(x, cmprdata[0])), np.transpose(np.append(x, cmprdata[1])), np.transpose(np.append(x, cmprdata[2])), np.transpose(np.append(x, cmprdata[3])), np.transpose(np.append(x, cmprdata[4])), np.transpose(np.append(x, cmprdata[5]))], NNmodel, scaler_x, scaler_y)
            else:
                preds = []
                for i in range(6):
                    preds.append(Matlab_Sampling.get_KM3_sample([np.transpose(np.append(x, cmprdata[0])), np.transpose(np.append(x, cmprdata[1])), np.transpose(np.append(x, cmprdata[2])), np.transpose(np.append(x, cmprdata[3])), np.transpose(np.append(x, cmprdata[4])), np.transpose(np.append(x, cmprdata[5]))][i].tolist()))
            curr_err = calc_error_6sets(preds, data_out, outputs)
            print(curr_err)
            rRMSEs.append(curr_err)
        print(rRMSEs)
        fits["rRMSE"] = rRMSEs
        filtered_fits = fits[fits["rRMSE"] < rRMSEth]
        filtered_fits = filtered_fits[filtered_fits["acX"] < 1]

    filtered_fits.to_csv("filtered_fit_ensemble.csv")
    return

#The tranistion model defines how to move from sigma_current to sigma_new
def transition_model(xs, sigma, multi=0):
    if multi == 0:
        x_new = []
        for x in xs[:-9]:
            x_new.append(x + np.random.normal(0, sigma) * x)
        for x in xs[-9:]:
            x_new.append(x)
        return np.asarray(x_new)

    else:
        x_new = []
        for x in xs:
            x_new.append(x + np.random.normal(0, sigma) * x)
        return np.asarray(x_new)

def prior(x, multi=0, verbose=0):
    #x[0] = mu, x[1]=sigma (new or current)
    #returns 1 for all valid values of sigma. Log(1) =0, so it does not affect the summation.
    #returns 0 for all invalid values of sigma (<=0). Log(0)=-infinity, and Log(negative number) is undefined.
    #It makes the new sigma infinitely unlikely.

    # if(x[1] <=0):
    #     return 0
    # return 1

    if multi == 1:
        if (10 ** -15) < x[0][0] < (10 ** -8):
            if (10 ** -20) < x[1][0] < (10 ** -11):
                if (10 ** -11) < x[2][0] < (10 ** -5):
                    if (10 ** -12) < x[3][0] < (10 ** -6):
                        if (10 ** -5.3) < x[4][0] < (10 ** -2.3):
                            if (10 ** -9) < x[5][0] < (10 ** -2):
                                if (10 ** -4) < x[6][0] < 1:
                                    return 1
                                else:
                                    if verbose == 1:
                                        print("Prior 7 rejected")
                            else:
                                if verbose == 1:
                                    print("Prior 6 rejected")
                        else:
                            if verbose == 1:
                                print("Prior 5 rejected")
                    else:
                        if verbose == 1:
                            print("Prior 4 rejected")
                else:
                    if verbose == 1:
                        print("Prior 3 rejected")
            else:
                if verbose == 1:
                    print("Prior 2 rejected")
        else:
            if verbose == 1:
                print("Prior 1 rejected")


    else:
        if (10**-15) < x[0] < (10**-8):
            if (10 ** -20) < x[1] < (10 ** -11):
                if (10 ** -11) < x[2] < (10 ** -5):
                    if (10 ** -12) < x[3] < (10 ** -6):
                        if (10 ** -5.3) < x[4] < (10 ** -2.3):
                            if (10 ** -9) < x[5] < (10 ** -2):
                                if (10 ** -4) < x[6] < 1:

                                    return 1
    #print("Prior rejected")
    return 0


#Computes the likelihood of the data given a sigma (new or current) according to equation (2)
def manual_log_like_normal(x,data):
    #x[0]=mu, x[1]=sigma (new or current)
    #data = the observation

    return np.sum(-np.log(x[1] * np.sqrt(2* np.pi) )-((data-x[0])**2) / (2*x[1]**2))

#Same as manual_log_like_normal(x,data), but using scipy implementation. It's pretty slow.
def log_lik_normal(x,d, outputs, multi=0):
    #x[0]=mu, x[1]=sigma (new or current)
    # data = the observation

    data = []
    for row in d:
        dr = list(row)
        data.append(dr)


    if multi == 0:
        if outputs == 9:
            first = np.log(scipy.stats.norm(x[0][0]).pdf(data[0][0]))
            second = np.log(scipy.stats.norm(x[0][1]).pdf(data[1][0]))
            third = np.log(scipy.stats.norm(x[0][2]).pdf(data[2][0]))
            fourth = np.log(scipy.stats.norm(x[0][3]).pdf(data[3][0]))
            fifth = np.log(scipy.stats.norm(x[0][4]).pdf(data[4][0]))
            sixth = np.log(scipy.stats.norm(x[0][5]).pdf(data[5][0]))
            seventh = np.log(scipy.stats.norm(x[0][6]).pdf(data[6][0]))
            eigth = np.log(scipy.stats.norm(x[0][7]).pdf(data[7][0]))
            ninth = np.log(scipy.stats.norm(x[0][8]).pdf(data[8][0]))
            return np.sum([abs(first), abs(second), abs(third), abs(fourth), abs(fifth), abs(sixth), abs(seventh), abs(eigth), abs(ninth)])
        else:
            first = scipy.stats.norm(data[0][0]).pdf(x[0][0])
            second = scipy.stats.norm(data[1][0]).pdf(x[0][1])
            third = scipy.stats.norm(data[2][0]).pdf(x[0][2])
            return np.sum([abs(first), abs(second), abs(third)])
    else:

        leaveout = []
        for n, dat in enumerate(data):
            while "no_data" in dat:
                leaveout.append([n, dat.index("no_data")])
                data[n][dat.index("no_data")] = 0

        first = []
        second = []
        third = []
        if outputs == 9:
            fourth = []
            fifth = []
            sixth = []
            seventh = []
            eigth = []
            ninth = []
        for i in range(6):
            first.append(abs(np.log(scipy.stats.norm(x[i][0]).pdf(data[i][0]))))
            second.append(abs(np.log(scipy.stats.norm(x[i][1]).pdf(data[i][1]))))
            third.append(abs(np.log(scipy.stats.norm(x[i][2]).pdf(data[i][2]))))
            if outputs == 9:
                fourth.append(abs(np.log(scipy.stats.norm(x[i][3]).pdf(data[i][3]))))
                fifth.append(abs(np.log(scipy.stats.norm(x[i][4]).pdf(data[i][4]))))
                sixth.append(abs(np.log(scipy.stats.norm(x[i][5]).pdf(data[i][5]))))
                seventh.append(abs(np.log(scipy.stats.norm(x[i][6]).pdf(data[i][6]))))
                eigth.append(abs(np.log(scipy.stats.norm(x[i][7]).pdf(data[i][7]))))
                ninth.append(abs(np.log(scipy.stats.norm(x[i][8]).pdf(data[i][8]))))
        if outputs != 9:
            for lo in leaveout:
                if lo[1] == 0:
                    del first[lo[0]]
                elif lo[1] == 1:
                    del second[lo[0]]
                elif lo[1] == 2:
                    del third[lo[0]]
        else:
            for lo in leaveout:
                if lo[1] == 0:
                    del first[lo[0]]
                elif lo[1] == 1:
                    del second[lo[0]]
                elif lo[1] == 2:
                    del third[lo[0]]
                elif lo[1] == 3:
                   del fourth[lo[0]]
                elif lo[1] == 4:
                   del fifth[lo[0]]
                elif lo[1] == 5:
                   del sixth[lo[0]]
                elif lo[1] == 6:
                   del seventh[lo[0]]
                elif lo[1] == 7:
                   del eigth[lo[0]]
                elif lo[1] == 8:
                   del ninth[lo[0]]

            # first = scipy.stats.norm(data[0][0]).pdf(x[0][0])
            # second = scipy.stats.norm(data[1][0]).pdf(x[0][1])
            # third = scipy.stats.norm(data[2][0]).pdf(x[0][2])
        if outputs == 9:
            return np.sum([np.average(first), np.average(second), np.average(third), np.average(fourth), np.average(fifth), np.average(sixth), np.average(seventh), np.average(eigth), np.average(ninth)])
        else:
            return np.sum([np.average(first), np.average(second), np.average(third)])
#Defines whether to accept or reject the new sample

def acceptance(x, x_new, multi=0):
    # if x_new<x:
    #     return True
    # else:
    #     accept=np.random.uniform(0,1)
    #     # Since we did a log likelihood, we need to exponentiate in order to compare to the random number
    #     # less likely x_new are less likely to be accepted
    #     return (accept < (x-x_new)/x)
    accprob = x/x_new
    ps = []
    for p in range(7):
        ps.append(random.uniform(0, 2))
    accts = []
    for p in ps:
        if accprob > p:
            accts.append(1)
        else:
            accts.append(0)
    return accts

def metropolis_hastings(likelihood_computer,prior, transition_model, param_init,iterations,data,acceptance_rule, sigma, NNmodel, scaler_x, scaler_y,outputs):
    # likelihood_computer(x,data): returns the likelihood that these parameters generated the data
    # transition_model(x): a function that draws a sample from a symmetric distribution and returns it
    # param_init: a starting sample
    # iterations: number of accepted to generated
    # data: the data that we wish to model
    # acceptance_rule(x,x_new): decides whether to accept or reject the new sample
    x = param_init
    accepted = []
    accepted_y = []
    y = nn_model_pred(np.transpose(x), NNmodel, scaler_x, scaler_y)
    for i in range(iterations):
        x_new = transition_model(x, sigma)
        if prior(x_new) == 1:
            y_new = nn_model_pred(np.transpose(x_new), NNmodel, scaler_x, scaler_y)
            print("----------------------------------")
            print(y_new[0][0], y_new[0][1], y_new[0][2], y_new[0][3], y_new[0][4], y_new[0][5], y_new[0][6], y_new[0][7], y_new[0][8])
            if (1.3905 < y_new[0][0] < 1.5505) and (0.3791 < y_new[0][1] < 0.5191) and (0.0542 < y_new[0][2] < 0.1142): #accepted fits (declare input parameter?)
                print("FIT")
                accepted.append(x_new)
                accepted_y.append(y_new)
                x = x_new
            else:
                x_lik = likelihood_computer(y,data,outputs)
                x_new_lik = likelihood_computer(y_new,data, outputs)
                print(x_lik)

                if np.isinf(x_lik) and np.isinf(x_new_lik):
                    x = get_init_para()
                    y = nn_model_pred(np.transpose(x), NNmodel, scaler_x, scaler_y)
                    x_lik = likelihood_computer(y, data, outputs)

                else:
                    accts = acceptance_rule(x_lik ,x_new_lik)
                    for num in range(len(accts)):
                        if accts[num]==1:
                            x[num] = x_new[num]

    return accepted, accepted_y

def metropolis_hastings_multidata(likelihood_computer,prior, transition_model, param_init,iterations,data,acceptance_rule, sigma, NNmodel, scaler_x, scaler_y, rRMSE_acc, cmprdata, desiredfits, outputs, verbose=0):
    # likelihood_computer(x,data): returns the likelihood that these parameters generated the data
    # transition_model(x): a function that draws a sample from a symmetric distribution and returns it
    # param_init: a starting sample
    # iterations: number of accepted to generated
    # data: the data that we wish to model
    # acceptance_rule(x,x_new): decides whether to accept or reject the new sample
    x = param_init
    accepted = []
    accepted_y = []
    rRMSEs = []
    i = 0
    y = nn_model_pred([np.transpose(np.append(x, cmprdata[0])), np.transpose(np.append(x, cmprdata[1])),
                       np.transpose(np.append(x, cmprdata[2])), np.transpose(np.append(x, cmprdata[3])),
                       np.transpose(np.append(x, cmprdata[4])), np.transpose(np.append(x, cmprdata[5]))], NNmodel,
                      scaler_x, scaler_y)
    while i < iterations and len(accepted) < desiredfits:
        if verbose == 1:
            if (i%1000) == 0 and i > 0:
                print("Step " + str(i))
                try:
                    print("FITs: " + str(len(accepted)))
                    print("Current acceptance rate: " + str((len(accepted)/i)*100) + " %")
                except:
                    pass

        x_new = transition_model(x, sigma, 1)
        if prior(x_new, 1) == 1:
            i = i+1
            y_new = nn_model_pred([np.transpose(np.append(x_new, cmprdata[0])), np.transpose(np.append(x_new, cmprdata[1])), np.transpose(np.append(x_new, cmprdata[2])), np.transpose(np.append(x_new, cmprdata[3])), np.transpose(np.append(x_new, cmprdata[4])), np.transpose(np.append(x_new, cmprdata[5]))], NNmodel, scaler_x, scaler_y)
            rRMSE = calc_error_6sets(y_new, data, outputs)
            #print("----------------------------------")
            #print(y_new[0][0], y_new[0][1], y_new[0][2])

            if rRMSE < rRMSE_acc: #accepted fits (declare input parameter?)
                if verbose == 1:
                    print("FIT " + str(len(accepted)))
                    print("Current acceptance rate: " + str((len(accepted)/i)*100) + " %")
                accepted.append(x_new)
                accepted_y.append(y_new)
                rRMSEs.append(rRMSE)
                x = x_new
            else:
                x_lik = likelihood_computer(y, data, outputs, 1)
                x_new_lik = likelihood_computer(y_new, data, outputs, 1)
                #print(x_lik)

                if np.isinf(x_lik) and np.isinf(x_new_lik):
                    x = get_init_para()
                    y = nn_model_pred([np.transpose(np.append(x, cmprdata[0])), np.transpose(np.append(x, cmprdata[1])), np.transpose(np.append(x, cmprdata[2])), np.transpose(np.append(x, cmprdata[3])), np.transpose(np.append(x, cmprdata[4])), np.transpose(np.append(x, cmprdata[5]))], NNmodel, scaler_x, scaler_y)
                    x_lik = likelihood_computer(y, data, outputs, 1)

                else:
                    accts = acceptance_rule(x_lik ,x_new_lik, 1)
                    for num in range(len(accts)):
                        if accts[num]==1:
                            x[num] = x_new[num]

    return accepted, accepted_y, rRMSEs, i


def metropolis_hastings_multidata_KM(likelihood_computer, prior, transition_model, param_init, iterations, data,
                                  acceptance_rule, sigma, rRMSE_acc, desiredfits,
                                  outputs, nn_sugg, verbose=1):
    # likelihood_computer(x,data): returns the likelihood that these parameters generated the data
    # transition_model(x): a function that draws a sample from a symmetric distribution and returns it
    # param_init: a starting sample
    # iterations: number of accepted to generated
    # data: the data that we wish to model
    # acceptance_rule(x,x_new): decides whether to accept or reject the new sample

    suggested = 0

    all_RMSEs = []
    if len(nn_sugg) == 0:
        x = param_init
    else:
        x = nn_sugg.iloc[suggested].to_numpy().reshape((7,1))
        suggested = suggested + 1
        print("Using NN-suggested parameter set.")
    accepted = []
    accepted_y = []
    rRMSEs = []
    i = 0

    y = Matlab_Sampling.get_KM_SUB_sample(x)
    temp_rRMSE = calc_error_6sets(y, data, outputs)
    all_RMSEs.append(temp_rRMSE)
    print(temp_rRMSE)

    while i < iterations and len(accepted) < desiredfits:
        if verbose == 1:
            if (i % 1000) == 0 and i > 0:
                print("Step " + str(i))
                try:
                    print("FITs: " + str(len(accepted)))
                    print("Current acceptance rate: " + str((len(accepted) / i) * 100) + " %")
                except:
                    pass

        x_new = transition_model(x, sigma, 1)
        if prior(x_new, 1) == 1:
            i = i + 1
            y_new = Matlab_Sampling.get_KM_SUB_sample(x_new)
            rRMSE = calc_error_6sets(y_new, data, outputs)
            all_RMSEs.append(rRMSE)
            print(rRMSE)
            # print("----------------------------------")
            # print(y_new[0][0], y_new[0][1], y_new[0][2])

            if rRMSE < rRMSE_acc:  # accepted fits (declare input parameter?)
                if verbose == 1:
                    print("FIT " + str(len(accepted)))
                    print("Current acceptance rate: " + str((len(accepted) / i) * 100) + " %")
                accepted.append(x_new)
                accepted_y.append(y_new)
                rRMSEs.append(rRMSE)
                x = x_new
            else:
                x_lik = likelihood_computer(y, data, outputs, 1)
                x_new_lik = likelihood_computer(y_new, data, outputs, 1)
                # print(x_lik)

                if np.isinf(x_lik) and np.isinf(x_new_lik):
                    i = i + 1
                    if len(nn_sugg) == 0:
                        x = param_init
                    else:
                        x = nn_sugg.iloc[suggested].to_numpy().reshape((7,1))
                        suggested = suggested + 1
                        print("Using NN-suggested parameter set.")

                    y = Matlab_Sampling.get_KM_SUB_sample(x)
                    temp_rRMSE = calc_error_6sets(y, data, outputs)
                    all_RMSEs.append(temp_rRMSE)
                    print(temp_rRMSE)
                    x_lik = likelihood_computer(y, data, outputs, 1)

                else:
                    accts = acceptance_rule(x_lik, x_new_lik, 1)
                    for num in range(len(accts)):
                        if accts[num] == 1:
                            x[num] = x_new[num]

    return accepted, accepted_y, rRMSEs, i, all_RMSEs

def metropolis_hastings_multidata_multirun(likelihood_computer,prior, transition_model, param_init,iterations,data,acceptance_rule, sigma, NNmodel, scaler_x, scaler_y, rRMSE_acc, cmprdata, desiredfits, outputs, batchsize, verbose=0):
    # likelihood_computer(x,data): returns the likelihood that these parameters generated the data
    # transition_model(x): a function that draws a sample from a symmetric distribution and returns it
    # param_init: a starting sample
    # iterations: number of accepted to generated
    # data: the data that we wish to model
    # acceptance_rule(x,x_new): decides whether to accept or reject the new sample
    xs = []
    for b in range(batchsize):
        xs.append(param_init)
    accepted = []
    accepted_y = []
    rRMSEs = []
    i = 0

    second_bs = []

    xs_sys = []
    for b in range(batchsize):
        xs_sys.append([np.transpose(np.append(xs[b], cmprdata[0])), np.transpose(np.append(xs[b], cmprdata[1])),
                       np.transpose(np.append(xs[b], cmprdata[2])), np.transpose(np.append(xs[b], cmprdata[3])),
                       np.transpose(np.append(xs[b], cmprdata[4])), np.transpose(np.append(xs[b], cmprdata[5]))])

    xs_m = np.array(xs_sys).reshape((batchsize * 6, 10))

    ys = nn_model_pred(xs_m, NNmodel, scaler_x, scaler_y)

    while i < iterations and len(accepted) < desiredfits:
        if verbose == 1:
            print("Step " + str(i))
            try:
                print("FITs: " + str(len(accepted)))
                print("Current acceptance rate: " + str((len(accepted)/(i*batchsize))*100) + " %")
            except:
                pass

        x_news = []
        for b in range(batchsize):
            ok = 0
            while ok == 0:
                x_n = transition_model(xs[b], sigma, 1)
                if prior(x_n, 1) == 1:
                    x_news.append(x_n)
                    ok = 1

        x_news_sys = []
        for b in range(batchsize):
            x_news_sys.append([np.transpose(np.append(x_news[b], cmprdata[0])), np.transpose(np.append(x_news[b], cmprdata[1])), np.transpose(np.append(x_news[b], cmprdata[2])), np.transpose(np.append(x_news[b], cmprdata[3])), np.transpose(np.append(x_news[b], cmprdata[4])), np.transpose(np.append(x_news[b], cmprdata[5]))])

        x_news_m = np.array(x_news_sys).reshape((batchsize * 6, 10))

        y_news = nn_model_pred(x_news_m, NNmodel, scaler_x, scaler_y)

        i = i + 1
        for b in range(batchsize):
            y_new = y_news[b*6:b*6+6]
            y = ys[b*6:b*6+6]
            x_new = x_news[b]
            x = xs[b]

            rRMSE = calc_error_6sets(y_new, data, outputs)
            #print("----------------------------------")
            #print(y_new[0][0], y_new[0][1], y_new[0][2])

            if rRMSE < rRMSE_acc: #accepted fits (declare input parameter?)
                if verbose == 1:
                    print("FIT " + str(len(accepted)))
                    print("Current acceptance rate: " + str((len(accepted)/(i*batchsize))*100) + " %")
                accepted.append(x_new)
                accepted_y.append(y_new)
                rRMSEs.append(rRMSE)
                xs[b] = x_new
            else:
                x_lik = likelihood_computer(y, data, outputs, 1)
                x_new_lik = likelihood_computer(y_new, data, outputs, 1)
                #print(x_lik)

                if np.isinf(x_lik) and np.isinf(x_new_lik):
                    second_bs.append(b)
                    xs[b] = get_init_para()
                    y = nn_model_pred([np.transpose(np.append(x, cmprdata[0])), np.transpose(np.append(x, cmprdata[1])), np.transpose(np.append(x, cmprdata[2])), np.transpose(np.append(x, cmprdata[3])), np.transpose(np.append(x, cmprdata[4])), np.transpose(np.append(x, cmprdata[5]))], NNmodel, scaler_x, scaler_y)
                    x_lik = likelihood_computer(y, data, outputs, 1)

                else:
                    accts = acceptance_rule(x_lik ,x_new_lik, 1)
                    for num in range(len(accts)):
                        if accts[num]==1:
                            xs[b][num] = x_news[b][num]

    return accepted, accepted_y, rRMSEs, i


def lognuniform(low=0, high=1, size=None, base=np.e):
    return np.power(base, np.random.uniform(low, high, size))


def get_init_para():
    #set bounds for sampling
    kslr = lognuniform(-15, -8, 1, 10)
    kbr = lognuniform(-20, -11, 1, 10)
    Dx = lognuniform(-11, -5, 1, 10)
    Dy = lognuniform(-12, -6, 1, 10)
    H_cp = lognuniform(-5.3, -2.3, 1, 10)
    Td = lognuniform(-9, -2, 1, 10)
    a0 = lognuniform(-4, 0, 1, 10)
    #rout = [0.0000325 for x in range(1)]
    #Xg = [2.76 * 10 ** 15 for x in range(1)]
    #iniconc = [1.897 * 10 ** 21 for x in range(1)]

    initial_params = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0])

    return initial_params


def perform_MCMC(sigma, iterations, model, scaler, rRMSE_acc, cmprdata, desiredfits=0, multi=1, outputs=3, batchsize=1, nn_sugg=[]):

    initial_params = get_init_para()

    if not "KM_SUB" in model:
        # LOAD NN FORWARD MODEL (3 outputs)
        NNmodel = keras.models.load_model(model)

        # LOAD x- and y-scaler associated to this model
        [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))

        if multi == 0:
            accepted, accepted_y = metropolis_hastings(log_lik_normal, prior, transition_model, initial_params,
                                                       iterations,
                                                       [[1.4705, 0.08], [0.4491, 0.07], [0.0842, 0.03]], acceptance,
                                                       sigma, NNmodel, scaler_x, scaler_y, outputs)
        else:
            if batchsize == 1:
                accepted, accepted_y, rRMSEs, i = metropolis_hastings_multidata(log_lik_normal, prior, transition_model,
                                                                                initial_params, iterations,
                                                                                cmprdata[1], acceptance, sigma,
                                                                                NNmodel, scaler_x, scaler_y, rRMSE_acc,
                                                                                cmprdata[0], desiredfits, outputs)
            else:
                accepted, accepted_y, rRMSEs, i = metropolis_hastings_multidata_multirun(log_lik_normal, prior,
                                                                                         transition_model,
                                                                                         initial_params, iterations,
                                                                                         cmprdata[1], acceptance, sigma,
                                                                                         NNmodel, scaler_x, scaler_y,
                                                                                         rRMSE_acc,
                                                                                         cmprdata[0], desiredfits,
                                                                                         outputs,
                                                                                         batchsize)

    else:
        accepted, accepted_y, rRMSEs, i, all_rRMSEs = metropolis_hastings_multidata_KM(log_lik_normal, prior, transition_model,
                                                                        initial_params, iterations,
                                                                        cmprdata[1], acceptance, sigma, rRMSE_acc, desiredfits, outputs, nn_sugg)

        return [i*batchsize, len(accepted), all_rRMSEs]




    print("Accepted parameter sets: ", str(len(accepted)))

    acc_list = []
    for rownum, row in enumerate(accepted):
        sublist = []
        for num, el in enumerate(row.tolist()):
            if num < 7:
                sublist.append(el[0])
        sublist.append(rRMSEs[rownum])
        acc_list.append(sublist)
    acc_y_list = []
    for row in accepted_y:
        sublist = row[0].tolist()

        acc_y_list.append(sublist)

    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX", "rRMSE"]
    with open("fits.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in acc_list:
        with open("fits.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    df = pd.read_csv("fits.csv")
    logdf = np.log10(df)
    g = sns.pairplot(logdf)
    plt.show()

    #header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX", "rRMSE", "out1", "out2", "out3", "out4", "out5", "out6", "out7", "out8", "out9"]
    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX", "rRMSE", "out1", "out2", "out3"]
    with open("fits_"+ str(sigma) + ".csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for num, row in enumerate(acc_list):
        for elem in acc_y_list[num]:
            row.append(elem)
        with open("fits_"+ str(sigma) + ".csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)


    return [i*batchsize, len(accepted)]



def perform_loguniform_sampling(model, scaler, n_fits, it_samples, rRMSE_lim, cmprdata_out, cmprdata_in, outputs=3, verbose=0):

    testedsamples = 0

    # LOAD NN FORWARD MODEL (3 outputs)
    NNmodel = keras.models.load_model(model)

    # LOAD x- and y-scaler associated to this model
    [scaler_x, scaler_y] = pickle.load(open(scaler, "rb"))


    goodfits = []

    while len(goodfits) < n_fits:
        testedsamples=testedsamples+it_samples
        samples = it_samples
        if verbose == 1:
            print("Testing " + str(samples))
            print("Found " + str(len(goodfits)))

        kslr = lognuniform(-15, -8, samples, 10)
        kbr = lognuniform(-20, -11, samples, 10)
        Dx = lognuniform(-11, -5, samples, 10)
        Dy = lognuniform(-12, -6, samples, 10)
        H_cp = lognuniform(-5.3, -2.3, samples, 10)
        Td = lognuniform(-9, -2, samples, 10)
        a0 = lognuniform(-4, 0, samples, 10)
        #rout = [0.0000325 for x in range(samples)]
        #Xg = [2.76*10**15 for x in range(samples)]
        #iniconc = [1.897*10**21 for x in range(samples)]

        df1 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[0][0] for x in range(it_samples)], [cmprdata_in[0][1] for x in range(it_samples)], [cmprdata_in[0][2] for x in range(it_samples)]]).transpose())
        df2 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[1][0] for x in range(it_samples)], [cmprdata_in[1][1] for x in range(it_samples)], [cmprdata_in[1][2] for x in range(it_samples)]]).transpose())
        df3 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[2][0] for x in range(it_samples)], [cmprdata_in[2][1] for x in range(it_samples)], [cmprdata_in[2][2] for x in range(it_samples)]]).transpose())
        df4 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[3][0] for x in range(it_samples)], [cmprdata_in[3][1] for x in range(it_samples)], [cmprdata_in[3][2] for x in range(it_samples)]]).transpose())
        df5 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[4][0] for x in range(it_samples)], [cmprdata_in[4][1] for x in range(it_samples)], [cmprdata_in[4][2] for x in range(it_samples)]]).transpose())
        df6 = pd.DataFrame(np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [cmprdata_in[5][0] for x in range(it_samples)], [cmprdata_in[5][1] for x in range(it_samples)], [cmprdata_in[5][2] for x in range(it_samples)]]).transpose())

        #params1 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [0.001 for x in range(samples)], [10000000000000 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params2 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [0.00002 for x in range(samples)], [69500000000000 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params3 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [0.0000325 for x in range(samples)], [2500000000000000 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params4 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [2.50E-05 for x in range(samples)], [2.00E+14 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params5 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [2.50E-05 for x in range(samples)], [3.25E+14 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        #params6 = np.array([kslr, kbr, Dx, Dy, H_cp, Td, a0, [2.50E-05 for x in range(samples)], [5.51E+14 for x in range(samples)], [1.89E+21 for x in range(samples)]])
        x_new = pd.concat((df1, df2, df3, df4, df5, df6), axis=0, ignore_index=1)
        y_new = nn_model_pred(x_new, NNmodel, scaler_x, scaler_y)
        rRMSEs = []
        for i in range(samples):
            rRMSEs.append(calc_error_6sets([y_new[i], y_new[i+it_samples], y_new[i+it_samples*2], y_new[i+it_samples*3], y_new[i+it_samples*4], y_new[i+it_samples*5]], cmprdata_out, outputs))

            #preds = nn_model_pred([np.transpose(params1)[i], np.transpose(params2)[i], np.transpose(params3)[i], np.transpose(params4)[i], np.transpose(params5)[i], np.transpose(params6)[i]], NNmodel, scaler_x, scaler_y)
            #rRMSEs.append(calc_error_6sets(preds, cmprdata_out))

        #hist for NN prediction errors
        #bins = 10 ** (np.arange(-2.2, 2, 0.1))
        #plt.hist(np.log10(rRMSEs), bins=bins)
        #plt.xscale("log")
        #plt.plot()

        for n, rRMSE in enumerate(rRMSEs):
            if rRMSE < rRMSE_lim:
                #row = list(np.transpose(x_new)[n][:-3])
                row = list(x_new.iloc[n, :])[:-3]
                row.append(rRMSE)
                goodfits.append(row)

    header = ["RS1", "RB1", "dXY", "dYY", "HXY", "deX", "acX", "rRMSE"]
    with open("loguniform_fits.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in goodfits:
        with open("loguniform_fits.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)


    df = pd.read_csv("loguniform_fits.csv")
    logdf = np.log10(df)
    g = sns.pairplot(logdf)
    plt.show()

    return [testedsamples, len(goodfits)]



def nn_model_pred(inpts, NNmodel, scaler_x, scaler_y, log=0):

    inpts_log = np.log10(inpts)
    inpts_t = scaler_x.transform(inpts_log)
    pred = NNmodel.predict(inpts_t)
    pred_t = scaler_y.inverse_transform(pred)
    pred_f = 10**pred_t

    if log==0:
        return pred_f
    else:
        return pred_t


def compare_to_data(preds):
    returns = []
    for num, pred in enumerate(preds):
        if (1.3905 < pred[0] < 1.5505) and (0.3791 < pred[1] < 0.5191) and (0.0542 < pred[2] < 0.1142):
            returns.append(num)

    return returns


def calc_error_6sets(preds, data, outputs, logabs=1):
    "Calculates relative root mean square error (logabs=0) or mean squared absolute logarithmic error (logabs=1) in comparison with 6 data sets"

    d = []
    for row in data:
        dr = list(row)
        d.append(dr)

    preds_l = []
    for row in preds:
        try:
            preds_l.append(row.tolist())
        except:
            preds_l.append(row)
    leaveout = []
    for n, dat in enumerate(d):
        while "no_data" in dat:
            leaveout.append([n, dat.index("no_data")])
            del preds_l[n][dat.index("no_data")]
            del d[n][dat.index("no_data")]

    if logabs == 1:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((np.log10(preds_l[i][j]) - np.log10(d[i][j])) ** 2)
            J.append(np.average(N))
        rRMSE = np.average(J)

    else:
        J = []
        for i, idat in enumerate(d):
            N = []
            for j, jdat in enumerate(idat):
                N.append((((preds_l[i][j] - d[i][j])/d[i][j])**2)/len(d[i]))
            J.append(np.sqrt(np.sum(N)))
        rRMSE = np.sum(J)

    return rRMSE