Materials for the work "Accelerating models for multiphase chemical kinetics through machine learning". Contains training and test data and scripts for pre-processing, model training and analysis.

Collaboration between:

Max Planck Institute for Chemistry, Hahn-Meitner-Weg 1, 55128 Mainz, Germany

Institute for Atmospheric and Climate Science, ETH Z�rich, 8092 Z�rich, Switzerland

Authors:

Thomas Berkemeier, Matteo Kr�ger, Aryeh Feinberg, Marcel M�ller, Ulrich P�schl and Ulrich K. Krieger

Code contributions:

Neural Network code by Matteo Kr�ger

Polynomial chaos expansion code by Aryeh Feinberg and Marcel M�ller